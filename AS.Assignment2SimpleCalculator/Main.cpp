//Simple Calculator
//Tony Skoviak

#include <iostream>
#include <conio.h>

using namespace std;
float Add(float num1, float num2);
float Subtract(float num1, float num2);
float Multiply(float num1, float num2);
bool Divide(float num1, float num2, float &answer);
float Pow(float num1, int num2);
 

int main()
{
	char input;
	float num1;
	float num2;
	char operation;
	float answer;

	cout << "This program will prompt you for two positive numbers and then a mathematical operator.\nWould you like to proceed (Y/N)?";
	cin >> input;

	if (input == 'y' || input == 'Y')
		while (true)
		{
			cout << "Please enter a positive number:";
			cin >> num1;

			cout << "Please enter another positive number:";
			cin >> num2;

			cout << "Please specify an operator (+, -, *, /, or ^):";
			cin >> operation;

			cout << "Equals: ";

			if (operation == '+')
			{
				
				cout << Add(num1, num2);
			}

			else
			{
				if (operation == '-')
				{
					cout << Subtract(num1, num2);
				}

				else
				{
					if (operation == '*')
					{
						cout << Multiply(num1, num2);
					}

					else
					{
						if (operation == '/')
						{
							Divide(num1, num2, answer);
							cout << answer;
						}

						else
						{
							if (operation == '^')
							{
								cout << Pow(num1, num2);
							}

							else
							{
								cout << "Invalid input.\nTry again? (Y/N) ";
								cin >> input;
								if (input == 'n' || input == 'N') break;
							}
						}
					}
				}
			}	
			cout << "\nAgain? (Y/N) ";
			cin >> input;
			if (input == 'n' || input == 'N') break;
		}
return 0;

}

//addition function
float Add(float num1, float num2)
{
	return  num1 + num2;
}

//subtraction function
float Subtract(float num1, float num2)
{
	return  num1 - num2;
}

//multiplication function
float Multiply(float num1, float num2)
{
	return  num1 * num2;
}

//division function
bool Divide(float num1, float num2, float &answer)
{
	answer = 0;
	
	//I don't think I'm doing this quite right; printing out the answer variable means that even if the division function returns false, the answer variable is printed—I set answer to 0 as a quick fix when dividing by 0
	if (num2 == 0)
		return false;
	else 
	{
		answer = num1 / num2;
		return true;
	}
}

//exponentiation function
float Pow(float num1, int num2)
{
	//If an exponent is 0, the return is always 1
	if (num2 == 0) num1 = 1;
	//Else, if an exponent is not 0, count the number of times the initial number needs to be multiplied by itself and then do so
	else 
	{
		num2--;
		if (num2 > 0)
		{
			num1 *= Pow(num1, num2);
		}
	}
	return num1;
}